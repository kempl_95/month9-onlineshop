'use strict';

//--< ============================================================== Константы ====================================================================
const baseUrl = 'http://localhost:9999';
const search = document.getElementById('search_form');
//--> ============================================================== Константы ====================================================================
search.addEventListener('submit', onSearchHandler);
function onSearchHandler(e) {
    e.preventDefault();
    const form = e.target;
    const formData = new FormData(form);
    let searchText = formData.get("text");
    /*   fetch(`${baseUrl}/search/${searchText}`).then();

       document.getElementById('product_list').innerHTML = '';
       document.getElementById('pages').hidden = true;
       searchProducts("/search/" + searchText);
}
function searchProducts(url) {
    const productTemplate = (product) =>
        `<div class="product">
             <div class="product_img"><img src="/img/${product.image}" alt=""></div>
             <div class="product_name"><h3>${product.name}</h3></div>
             <p>${product.price} сом</p>
             <button class="header_btn">В корзину</button>
         </div>
    `.trim();
    const fetchProducts = async (page, size) => {
        const productsPath = `${baseUrl}/products${url}?page=${page}&size=${size}`;
        const data = await fetch(productsPath, {cache: 'no-cache'});
        return data.json();
    };
    async function addSearchedProducts(page) {
        const data = await fetchProducts(page, 10);
        const list = document.getElementById('product_list');
        document.getElementById('product_list').innerHTML = '';
        for (let item of data.content) {
            const li = document.createElement('div');
            li.innerHTML = productTemplate(item);
            list.append(li.children[0]);
        }
        if (!data.last){
            document.getElementById('pages').hidden = false;
            document.getElementById('loadPrev').hidden = true;
            loadProductsPageable(url);
        }
    }
    addSearchedProducts(0).then();

*/
}
//--< ============================================================== Основная функция ====================================================================
const getCurrentPage = () => {
    const loc = (typeof window.location !== 'string') ? window.location.search : window.location;
    const index = loc.indexOf('page=');
    return index === -1 ? 1 : parseInt(loc[index + 5]) + 1;
};

(function loadPlacesPageable() {
    const productTemplate = (product) =>
        `<div class="product">
             <div class="product_img"><img src="/img/${product.image}" alt=""></div>
             <div class="product_name"><h3>${product.name}</h3></div>
             <p>${product.price} сом</p>
             <button class="header_btn">В корзину</button>
         </div>
    `.trim();

    const fetchPlaces = async (page, size) => {
        const placesPath = `${baseUrl}/products?page=${page}&size=${size}`;
        const data = await fetch(placesPath, {cache: 'no-cache'});
        return data.json();
    };

    const loadNextProductsGenerator = (loadNextElement, page) => {
        return async (event) => {
            event.preventDefault();
            event.stopPropagation();

            const defaultPageSize = loadNextElement.getAttribute('data-default-page-size');
            const data = await fetchPlaces(page, defaultPageSize);

            loadNextElement.hidden = data.length === 0;
            if (data.length === 0) {
                return;
            }

            const list = document.getElementById('product_list');
            list.innerHTML = '';
            for (let item of data.content) {
                list.innerHTML = productTemplate(item);
            }

            loadNextElement.addEventListener('click', loadNextProductsGenerator(loadNextElement, page + 1), {once: true});
        };
    };
    document.getElementById('loadPrev').hidden = true;
    const loadNextElement = document.getElementById('loadNext');
    if (loadNextElement !== null) {
        loadNextElement.innerText = "Следующая страница >";
        loadNextElement.addEventListener('click', loadNextProductsGenerator(loadNextElement, getCurrentPage()), {once: true});
    }
})();
//--< ============================================================== Listeners ====================================================================
for (let i = 0; i<document.getElementsByClassName("category_item").length; i++){
    document.getElementsByClassName("category_item")[i].addEventListener('click', function () {
        productsByCategoryGet(fetchProductsByCategory(document.getElementsByClassName("category_item")[i].getAttribute('categoryId'), 0));
    });
}

