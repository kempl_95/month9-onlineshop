'use strict';
const baseUrl = 'http://localhost:9999';
let email;
    //--< ***************** Восстановление пароля *****************************
async function fetchToken(e) {
    e.preventDefault();
    e.stopPropagation();
    const form = document.getElementById("emulation_form");
    const data = new FormData(form);
    const dataJSON = JSON.stringify(Object.fromEntries(data));
    email = data.get("email");
    const options = {
        method : 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: dataJSON
    };

    await fetch("/emulation", options);
}
async function fetchResetPassword(e) {
    e.preventDefault();
    e.stopPropagation();
    const form = document.getElementById("reset_form");
    const data = new FormData(form);
    const dataJSON = JSON.stringify(Object.fromEntries(data));

    const options = {
        method : 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: dataJSON
    };

    await fetch("/reset", options);
}
    //--> ***************** Восстановление пароля *****************************


//--> ============================================================== Запросы ========================================================================
//--< ============================================================== Обновляю ==============================================================
function update() {
    if (document.getElementsByClassName("emulation_form_block_btn")[0] !== undefined) {
        document.getElementsByClassName("emulation_form_block_btn")[0].addEventListener('click', function (e) {
            fetchToken(e).then(res => {
                window.location.href = `${baseUrl}/reset_password/${email}`;
            });
        });
    }
    if (document.getElementsByClassName("reset_block_btn")[0] !== undefined) {
        document.getElementsByClassName("reset_block_btn")[0].addEventListener('click', function (e) {
            fetchResetPassword(e).then(res => {
                window.location.href = `${baseUrl}/login`;
            });
        });
    }
}
//update();
//--> ============================================================== Обновляю ==============================================================
