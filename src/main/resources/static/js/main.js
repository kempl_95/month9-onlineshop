'use strict';
//--< ============================================================== Константы ====================================================================
const baseUrl = 'http://localhost:9999';
let pageParam;
let searchText;
let searchType;
let clickedCategory;
let clickedProductId = '';
const search = document.getElementById("search_form");
const fullSearchLink = document.getElementById("searchLink");
const fullSearch = document.getElementById("full_search");
const categoriesBlock = document.getElementById("categories_list");
const regForm = document.getElementById("registration");
const profileLinks = document.getElementById("profile_links");
const loginLink = document.getElementById("log_link");
const registrationLink = document.getElementById("reg_link");
const loginedBlock = document.getElementById("logined");
const unloginedBlock = document.getElementById("not_logined");
const logOut = document.getElementById("log_out");
const regBack = document.getElementsByClassName("back");
const regBack2 = document.getElementsByClassName("form_back");
const productList = document.getElementById("product_list");
const pageLinks = document.getElementById("product_page_links");
const basketLink = document.getElementById('basket');
//--> ============================================================== Константы ====================================================================
//--< ============================================================== Объекты ======================================================================
function PageParam(first, last, number, size, previous, next) {
    this.first = first;
    this.last = last;
    this.number = number;
    this.size = size;
    this.previous = previous;
    this.next = next;
}
function Product(id, name, image, qty, description, price) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.qty = qty;
    this.description = description;
    this.price = price;
}
function Page(id, className, text) {
    this.id = id;
    this.className = className;
    this.text = text;
}
function Basket(session ,products, sum, temp_user_id, user_id) {
    this.session = session;
    this.products = products;
    this.sum = sum;
    this.temp_user_id = temp_user_id;
    this.user_id = user_id;
}
function Token(name, token) {
    this.name = name;
    this.token = token;
}
//--> ============================================================== Объекты ========================================================================
//--< ============================================================== Разные функции =================================================================
function saveUser(user) {
    const userAsJSON = JSON.stringify(user);
    localStorage.setItem('user', userAsJSON);
}
function restoreUser() {
    const userAsJSON = localStorage.getItem('user');
    return JSON.parse(userAsJSON);
}
function updateOptions(options) {
    const update = { ...options };
    update.mode = 'cors';
    update.headers = { ... options.headers };
    update.headers['Content-Type'] = 'application/json';
    const user = restoreUser();
    if(user) {
        update.headers['Authorization'] = 'Basic ' + btoa(user.login + ':' + user.password);
    }
    return update;
}
function clearLocalStorage(){
    localStorage.clear();
    //при выходе обновить
}
function addElement(dom_element, adding_element){
    dom_element.append(adding_element);
}
//Проверка, если в хранилище есть данные, то ввести их, иначе просто вывести лог/рег
function checkLocalStorageForUserData() {
    if (localStorage.getItem('user') == null){

    } else {
        //добавить если зарегался
        logOut.addEventListener('click', clearLocalStorage);
    }
}
//--> ============================================================== Разные функции =================================================================
//--< ============================================================== Запросы ========================================================================
    //--< ***************** Поиск ***********************
search.addEventListener('submit', onSearchHandler);
async function onSearchHandler(e) {
    e.preventDefault();
    const form = e.target;
    const formData = new FormData(form);
    searchText = formData.get("text");
    productList.innerHTML = '';
    pageLinks.innerHTML = '';
    productSearchGet(searchFetch(searchText, 0, 5));
}
const searchFetch = async (text, page) => {
    const searchPath = `/products/search/${text}?page=${page}&size=${pageParam.size}`;
    const data = await fetch(searchPath, {cache: 'no-cache'});
    return data.json();
};
function productSearchGet(fetch) {
    fetch.then(data => {
        pageParam = new PageParam(data.first, data.last, data.number,10, data.number-1, data.number+1);
        productList.innerHTML = '';
        for (let i = 0; i<data.content.length; i++){
            addElement(productList, createProductElement(new Product(data.content[i].id, data.content[i].name, data.content[i].image, data.content[i].qty, data.content[i].description, data.content[i].price)));
        }
        // make pageClickers
        if (pageParam.first&&!pageParam.last){
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('next_product_page', 'next_page', '>')));
            document.getElementById('next_product_page').addEventListener('click', function () {
                productSearchGet(searchFetch(searchText, pageParam.next));
            });
        } else if (!pageParam.first&&pageParam.last){
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('previous_product_page', 'previous_page', '<')));
            document.getElementById('previous_product_page').addEventListener('click', function () {
                productSearchGet(searchFetch(searchText, pageParam.previous));
            });
        } else if (pageParam.last&&pageParam.first){
            pageLinks.innerHTML = '';
        } else {
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('previous_product_page', 'previous_page', '<')));
            document.getElementById('previous_product_page').addEventListener('click', function () {
                productSearchGet(searchFetch(searchText, pageParam.previous));
            });
            addElement(pageLinks ,createPageLinkElement(new Page('next_product_page', 'next_page', '>')));
            document.getElementById('next_product_page').addEventListener('click', function () {
                productSearchGet(searchFetch(searchText, pageParam.next));
            });
        }
        document.getElementById('full_search').innerHTML = '';
        update();
    });
}
    //--> ***************** Поиск ***********************
    //--< ***************** Расширеный поиск ***********************
fullSearchLink.addEventListener('click', onFullSearchHandler);
function onFullSearchHandler() {
    productList.innerHTML = '';
    productList.style.padding = '0';
    pageLinks.innerHTML = '';
    fullSearch.innerHTML =
        `<div class="full_search_item">`+
            `<h3>Подробный поиск</h3>`+
            `<form id="full_search_form" class="full_search_form">`+
                `<h4>По имени товара: </h4>`+
                `<input type="text" class="field" name="name" placeholder="Имя">`+
                `<h4>По описанию товара: </h4>`+
                `<input type="text" class="field" name="description" placeholder="Описание">`+
                `<h4>По цене товара: </h4>`+
                `<input type="text" class="field" name="price" placeholder="Цена">`+
                `<button id="full_search_btn" class="full_search_btn" type="submit">Искать</button>`+
            `</form>`+
        `</div>`
    ;
    document.getElementById('full_search_form').addEventListener('submit', fullSearchHandler);
}
const fullSearchFetch = async (type ,text, page) => {
    const searchPath = `/products/search/${type}/${text}?page=${page}&size=${pageParam.size}`;
    const data = await fetch(searchPath, {cache: 'no-cache'});
    return data.json();
};
function fullSearchHandler(e) {
    e.preventDefault();
    const form = e.target;
    const data = new FormData(form);
    if (data.get('name') != null && data.get('name') !== undefined && data.get('name') !== ''){
        searchText = data.get("name");
        searchType = 'name';
        productFullSearchGet(fullSearchFetch(searchType, searchText, 0, 5));
    } else if(data.get('description') != null && data.get('description') !== undefined && data.get('description') !== '') {
        searchText = data.get("description");
        searchType = 'description';
        productFullSearchGet(fullSearchFetch(searchType, searchText, 0, 5));
    } else if(data.get('price') != null && data.get('price') !== undefined && data.get('price') !== '') {
        searchText = data.get("price");
        searchType = 'price';
        productFullSearchGet(fullSearchFetch(searchType, searchText, 0, 5));
    }
}
function productFullSearchGet(fetch) {
    fetch.then(data => {
        pageParam = new PageParam(data.first, data.last, data.number,10, data.number-1, data.number+1);
        productList.innerHTML = '';
        for (let i = 0; i<data.content.length; i++){
            addElement(productList, createProductElement(new Product(data.content[i].id, data.content[i].name, data.content[i].image, data.content[i].qty, data.content[i].description, data.content[i].price)));
        }
        if (pageParam.first&&!pageParam.last){
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('next_product_page', 'next_page', '>')));
            document.getElementById('next_product_page').addEventListener('click', function () {
                productFullSearchGet(fullSearchFetch(searchType, searchText, pageParam.next));
            });
        } else if (!pageParam.first&&pageParam.last){
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('previous_product_page', 'previous_page', '<')));
            document.getElementById('previous_product_page').addEventListener('click', function () {
                productFullSearchGet(fullSearchFetch(searchType, searchText, pageParam.previous));
            });
        } else if (pageParam.last&&pageParam.first){
            pageLinks.innerHTML = '';
        } else {
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('previous_product_page', 'previous_page', '<')));
            document.getElementById('previous_product_page').addEventListener('click', function () {
                productFullSearchGet(fullSearchFetch(searchType, searchText, pageParam.previous));
            });
            addElement(pageLinks ,createPageLinkElement(new Page('next_product_page', 'next_page', '>')));
            document.getElementById('next_product_page').addEventListener('click', function () {
                productFullSearchGet(fullSearchFetch(searchType, searchText, pageParam.next));
            });
        }
        document.getElementById('full_search').innerHTML = '';
        update();
    });
}
    //--> ***************** Расширеный поиск ***********************
    //--< ***************** Products ******************
function createProductElement(product) {
    let productElement = document.createElement('div');
    productElement.className = "product";
    productElement.innerHTML =
        `<span id="product_id" style="display: none">${product.id}</span>`+
        `<div class="product_img"><a href="/products/${product.id}"><img src="/img/${product.image}" alt=""></a></div>`+
        `<div class="product_name"><a href="/products/${product.id}"><h3>${product.name}</h3></a></div>`+
        `<p>${product.price} сом</p>`+
        `<div class="product_basket">`+
            `<form class="product_basket_form">`+
                `<input type="hidden" name="qty" value="1" min="1" required>`+
                `<input type="hidden" name="product_id" value="${product.id}">`+
                `<div class="product_basket_btn_block w_100">`+
                    `<button type="submit" class="product_basket_btn">В корзину</button>`+
                `</div>`+
            `</form>`+
        `</div>`
    ;
    /* //При исправлении ошибки с csrf вернуть
    const _csrf = document.getElementsByName("_csrf")[0].content;
    const _csrf_token = document.getElementsByName("_csrf_token")[0].content;
    let token = new Token(_csrf, _csrf_token);
        let csrfElement = document.createElement('input');
        csrfElement.type = 'hidden';
        csrfElement.name = token.name;
        csrfElement.value = token.token;
        productElement.getElementsByClassName('product_basket_form')[0].append(csrfElement);
    */
    return productElement;
}
function createFullProductElement(product) {
    let productElement = document.createElement('div');
    productElement.className = "product";
    productElement.innerHTML =
        `<div class="product_img"><img src="/img/${product.image}" alt=""></div>`+
        `<div class="product_name"><h3>${product.name}</h3></div>`+
        `<p class="product_description">${product.description}</p>`+
        `<p>${product.price} сом</p>`+
        `<button class="header_btn">В корзину</button>`
    ;
    return productElement;
}
const fetchProducts = async (number) => {
    const productsPath = `${baseUrl}/products?page=${number}&size=${pageParam.size}`;
    const data = await fetch(productsPath, {cache: 'no-cache'});
    return data.json();
};
function productGet(fetch) {
    fetch.then(data => {
        pageParam = new PageParam(data.first, data.last, data.number,10, data.number-1, data.number+1);
        productList.innerHTML = '';
        for (let i = 0; i<data.content.length; i++){
            addElement(productList, createProductElement(new Product(data.content[i].id, data.content[i].name, data.content[i].image, data.content[i].qty, data.content[i].description, data.content[i].price)));
        }
        //  addPageElements
        if (pageParam.first&&!pageParam.last){
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('next_product_page', 'next_page', '>')));
            document.getElementById('next_product_page').addEventListener('click', function () {
                productGet(fetchProducts(pageParam.next));
            });
        } else if (!pageParam.first&&pageParam.last){
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('previous_product_page', 'previous_page', '<')));
            document.getElementById('previous_product_page').addEventListener('click', function () {
                productGet(fetchProducts(pageParam.previous));
            });
        } else if (pageParam.last&&pageParam.first){
            pageLinks.innerHTML = '';
        } else {
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('previous_product_page', 'previous_page', '<')));
            document.getElementById('previous_product_page').addEventListener('click', function () {
                productGet(fetchProducts(pageParam.previous));
            });
            addElement(pageLinks ,createPageLinkElement(new Page('next_product_page', 'next_page', '>')));
            document.getElementById('next_product_page').addEventListener('click', function () {
                productGet(fetchProducts(pageParam.next));
            });
        }
        update();
    });
}


    //--> ***************** Products ******************
    //--< ***************** Categories ******************

for (let i = 0; i<document.getElementsByClassName("category_item").length; i++) {
    document.getElementsByClassName("category_item")[i].addEventListener('click', function () {
        productsByCategoryGet(fetchProductsByCategory(document.getElementsByClassName("category_item")[i].getAttribute('categoryId'), 0));
    });
}
        //--< При клике на категорию
const fetchProductsByCategory = async (i, page) => {
    clickedCategory = i;
    const categoriesPath = `${baseUrl}/products/category/${clickedCategory}?page=${page}&size=${pageParam.size}`;
    const data = await fetch(categoriesPath, {cache: 'no-cache'});
    return data.json();
};
function productsByCategoryGet(fetch) {
    fetch.then(data => {
        pageParam = new PageParam(data.first, data.last, data.number,10, data.number-1, data.number+1);
        productList.innerHTML = '';
        fullSearch.innerHTML = '';
        for (let i = 0; i < data.content.length; i++) {
            addElement(productList, createProductElement(new Product(data.content[i].id, data.content[i].name, data.content[i].image, data.content[i].qty, data.content[i].description, data.content[i].price)));
        }
        if (pageParam.first&&!pageParam.last){
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('next_product_page', 'next_page', '>')));
            document.getElementById('next_product_page').addEventListener('click', function () {
                productsByCategoryGet(fetchProductsByCategory(clickedCategory, pageParam.next));
            });
        } else if (!pageParam.first&&pageParam.last){
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('previous_product_page', 'previous_page', '<')));
            document.getElementById('previous_product_page').addEventListener('click', function () {
                productsByCategoryGet(fetchProductsByCategory(clickedCategory, pageParam.previous));
            });
        } else if (pageParam.last&&pageParam.first){
            pageLinks.innerHTML = '';
        } else {
            pageLinks.innerHTML = '';
            addElement(pageLinks ,createPageLinkElement(new Page('previous_product_page', 'previous_page', '<')));
            document.getElementById('previous_product_page').addEventListener('click', function () {
                productsByCategoryGet(fetchProductsByCategory(clickedCategory, pageParam.previous));
            });
            addElement(pageLinks ,createPageLinkElement(new Page('next_product_page', 'next_page', '>')));
            document.getElementById('next_product_page').addEventListener('click', function () {
                productsByCategoryGet(fetchProductsByCategory(clickedCategory, pageParam.next));
            });
        }
        update();
    });
}
        //--> При клике на категорию
    //--> ***************** Categories ******************
    //--< ***************** Постраничные ссылки ******************
function createPageLinkElement(page) {
    pageLinks.style.display = 'inline-block';
    let pageLinkElement = document.createElement('span');
    pageLinkElement.id = page.id;
    pageLinkElement.className = "page " + page.className;
    pageLinkElement.innerText = page.text;
    return pageLinkElement;
}
    //--> ***************** Постраничные ссылки ******************
    //--< ***************** В корзину *****************************

async function fetchProductToBasket(e, i) {
    e.preventDefault();
    e.stopPropagation();
    const form = document.getElementsByClassName("product_basket_form")[i];
    const data = new FormData(form);
    const dataJSON = JSON.stringify(Object.fromEntries(data));
    const options = {
        method : 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: dataJSON
    };

    await fetch("/baskets", options);
}

    //--> ***************** В корзину *****************************
    //--< ***************** Удаление из корзины *****************************
async function fetchDeleteProductInBasket(e, i) {
    e.preventDefault();
    e.stopPropagation();
    const form = document.getElementsByClassName("pbd_form")[i];
    const data = new FormData(form);
    const dataJSON = JSON.stringify(Object.fromEntries(data));
    const options = {
        method : 'delete',
        headers: {
            'Content-Type': 'application/json'
        },
        body: dataJSON
    };

    await fetch("/baskets", options);
}
    //--> ***************** Удаление из корзины *****************************
    //--< ***************** Оформление заказа *****************************
async function fetchCreateOrder(e) {
    e.preventDefault();
    e.stopPropagation();
    const form = document.getElementById("bp_order_form");
    const data = new FormData(form);
    const dataJSON = JSON.stringify(Object.fromEntries(data));
    const options = {
        method : 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: dataJSON
    };

    await fetch("/orders", options);
}

    //--> ***************** Оформление заказа *****************************
    //--< ***************** Отзыв *****************************
async function fetchAddReview(e) {
    e.preventDefault();
    e.stopPropagation();
    const form = document.getElementById("review_form");
    const data = new FormData(form);
    const dataJSON = JSON.stringify(Object.fromEntries(data));
    clickedProductId = data.get('product_id');
    const options = {
        method : 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: dataJSON
    };
    await fetch("/reviews", options);
}

//--> ***************** Оформление заказа *****************************




//--> ============================================================== Запросы ========================================================================
//--< ============================================================== Обновляю ==============================================================
function update() {
    for (let idx = 0; idx<document.getElementsByClassName("product_basket_btn").length; idx++) {
        document.getElementsByClassName("product_basket_btn")[idx].addEventListener('click', function (e) {
            fetchProductToBasket(e, idx).then();
        });
    }
    for (let idx = 0; idx<document.getElementsByClassName("pbd_btn").length; idx++) {
        document.getElementsByClassName("pbd_btn")[idx].addEventListener('click', function (e) {
            fetchDeleteProductInBasket(e, idx).then(res => window.location.href = `${baseUrl}/basket`);
        });
    }
    if (document.getElementsByClassName("bp_order_btn")[0] !== undefined){
        document.getElementsByClassName("bp_order_btn")[0].addEventListener('click', function (e) {
            fetchCreateOrder(e).then(res => {
                window.location.href = `${baseUrl}/success_order`;
            });
        });
    }
    for (let idx = 0; idx<document.getElementsByClassName("review_block_btn").length; idx++) {
        if (document.getElementsByClassName("review_block_btn")[idx] !== undefined) {
            document.getElementsByClassName("review_block_btn")[idx].addEventListener('click', function (e) {
                fetchAddReview(e).then(res => {
                    window.location.href = `${baseUrl}/products/${clickedProductId}`;
                });
            });
        }
    }

}
function updateAnimation() {
    pageParam = new PageParam(true, false, 0,10, -1, 1);
    if (pageParam.first&&!pageParam.last){
        addElement(pageLinks ,createPageLinkElement(new Page('next_product_page', 'next_page', '>')));
        document.getElementById('next_product_page').addEventListener('click', function () {
            productGet(fetchProducts(pageParam.next));
        });
    } else if (pageParam.last&&pageParam.first){}
    //checkLocalStorageForUserData();
}
update();
updateAnimation();
//--> ============================================================== Обновляю ==============================================================
