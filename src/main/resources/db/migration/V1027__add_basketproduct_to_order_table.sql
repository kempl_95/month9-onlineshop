alter table orders
    add column basket_product_id int after basket_id,
    add index IDX_basket_product (basket_product_id),
    add FOREIGN KEY FK_basket_product (basket_product_id) REFERENCES basket_product (id);

