use onlineshop;

CREATE TABLE temp_user (
	id INT NOT NULL AUTO_INCREMENT primary key,
    number INT
);
CREATE TABLE basket_product (
	id INT NOT NULL AUTO_INCREMENT primary key,
    product_id int NOT NULL,
	qty int,
	sum double,
    INDEX IDX_product (product_id),
	FOREIGN KEY FK_product (product_id)
	REFERENCES product (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
);
CREATE TABLE basket (
	id INT NOT NULL AUTO_INCREMENT primary key,
	user_id int,
	temp_user_id int,
    basket_product_id int NOT NULL,
    order_id int,
	sum double,
    INDEX IDX_user (user_id),
	FOREIGN KEY FK_user (user_id)
	REFERENCES user (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE,
	INDEX IDX_temp_user (temp_user_id),
	FOREIGN KEY FK_temp_user (temp_user_id)
	REFERENCES temp_user (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE,
	INDEX IDX_basket_product (basket_product_id),
	FOREIGN KEY FK_basket_product (basket_product_id)
	REFERENCES basket_product (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE,
	INDEX IDX_order (order_id),
	FOREIGN KEY FK_order (order_id)
	REFERENCES orders (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
);
alter table orders
	add column temp_user_id int after user_id,
    add index IDX_temp_user (temp_user_id),
    add FOREIGN KEY FK_temp_user (temp_user_id) REFERENCES temp_user (id);