use onlineshop;

CREATE TABLE subcategory (
	id INT NOT NULL AUTO_INCREMENT primary key,
    name varchar(128),
	category_id int NOT NULL,
    product_id int,
    INDEX IDX_category (category_id),
	FOREIGN KEY FK_category (category_id)
	REFERENCES category (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE,
    INDEX IDX_product (product_id),
	FOREIGN KEY FK_product (product_id)
	REFERENCES product (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
);
alter table category
	add column subcategory_id int after product_id,
    add index IDX_subcategory (subcategory_id),
    add FOREIGN KEY FK_subcategory (subcategory_id) REFERENCES subcategory (id);
