ALTER TABLE `onlineshop`.`orders`
    DROP FOREIGN KEY `orders_ibfk_3`;
ALTER TABLE `onlineshop`.`orders`
    CHANGE COLUMN `temp_user_id` `session_id` INT(11) NULL DEFAULT NULL ;
ALTER TABLE `onlineshop`.`orders`
    ADD CONSTRAINT `orders_ibfk_3`
        FOREIGN KEY (`session_id`)
            REFERENCES `onlineshop`.`session` (`id`);
