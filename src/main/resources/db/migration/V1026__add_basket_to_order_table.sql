alter table orders
    add column basket_id int after product_id,
    add index IDX_basket (basket_id),
    add FOREIGN KEY FK_basket (basket_id) REFERENCES basket (id);

