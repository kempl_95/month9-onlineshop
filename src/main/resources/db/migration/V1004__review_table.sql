use onlineshop;

CREATE TABLE review (
	id INT NOT NULL AUTO_INCREMENT primary key,
    text varchar(512),
	user_id int NOT NULL,
    product_id int NOT NULL,
    INDEX IDX_user (user_id),
	FOREIGN KEY FK_user (user_id)
	REFERENCES user (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE,
    INDEX IDX_product (product_id),
	FOREIGN KEY FK_product (product_id)
	REFERENCES product (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
);
alter table user
	add column review_id int after order_id,
    add index IDX_review (review_id),
    add FOREIGN KEY FK_review (review_id) REFERENCES review (id);
alter table product
	add column review_id int after order_id,
    add index IDX_review (review_id),
    add FOREIGN KEY FK_review (review_id) REFERENCES review (id);