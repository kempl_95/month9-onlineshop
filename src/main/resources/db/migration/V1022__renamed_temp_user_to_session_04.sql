ALTER TABLE `onlineshop`.`basket_product`
    DROP FOREIGN KEY `basket_product_ibfk_3`;
ALTER TABLE `onlineshop`.`basket_product`
    CHANGE COLUMN `temp_user_id` `session_id` INT(11) NOT NULL ;
ALTER TABLE `onlineshop`.`basket_product`
    ADD CONSTRAINT `basket_product_ibfk_3`
        FOREIGN KEY (`session_id`)
            REFERENCES `onlineshop`.`session` (`id`);