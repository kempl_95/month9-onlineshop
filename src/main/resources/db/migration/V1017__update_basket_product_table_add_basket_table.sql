ALTER TABLE basket_product
    add column basket_id int after id,
    add index IDX_basket (basket_id),
    add FOREIGN KEY FK_basket (basket_id) REFERENCES basket (id);

ALTER TABLE product
    add column basket_product_id int after review_id,
    add index IDX_basket_product (basket_product_id),
    add FOREIGN KEY FK_basket_product (basket_product_id) REFERENCES basket_product (id);