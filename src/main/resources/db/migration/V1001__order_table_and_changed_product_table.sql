use onlineshop;

CREATE TABLE orders (
	id INT NOT NULL AUTO_INCREMENT primary key,
    sum int,
	DATETIME DATETIME,
    user_id int,
    product_id int,
    INDEX IDX_user (user_id),
	FOREIGN KEY FK_user (user_id)
	REFERENCES user (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE,
    INDEX IDX_product (product_id),
	FOREIGN KEY FK_product (product_id)
	REFERENCES product (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE
);
alter table user
	add column order_id int after surname,
    add index IDX_order (order_id),
    add FOREIGN KEY FK_order (order_id) REFERENCES orders (id);

alter table category
	add column product_id int after name,
    add index IDX_product (product_id),
    add FOREIGN KEY FK_product (product_id) REFERENCES product (id);
    
alter table product 
	add column order_id int after category_id,
    add index IDX_order (order_id),
    add FOREIGN KEY FK_order (order_id) REFERENCES orders (id);