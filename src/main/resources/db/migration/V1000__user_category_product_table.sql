use onlineshop;

CREATE TABLE user (
	id INT NOT NULL AUTO_INCREMENT primary key,
	login varchar(128),
    email varchar(128),
    password varchar(128),
    name varchar(128),
    surname varchar(128)
);
CREATE TABLE category (
	id INT NOT NULL AUTO_INCREMENT primary key,
    name varchar(128)
);
CREATE TABLE product (
	id INT NOT NULL AUTO_INCREMENT primary key,
    name varchar(255),
    image varchar(128),
    qty int,
	description varchar(255),
    price double,
    category_id int,
    INDEX IDX_category (category_id),
	FOREIGN KEY FK_category (category_id)
	REFERENCES category (id)
	ON DELETE RESTRICT
	ON UPDATE CASCADE,
    INDEX IDX_name (name),
    INDEX IDX_price (price)
);
-- Примеры insert:
-- insert into hw34t2.room values (1, 1), (2, 2);
-- insert into hw34t2.room_client values (1, 'Виктор'),(2, 'Анна'),(3, 'Николай'),(4, 'Юра'),(5, 'Айбек');
-- insert into hw34t2.room_status values (1, 'доступно'),(2, 'в использовании'),(3, 'забронировано');