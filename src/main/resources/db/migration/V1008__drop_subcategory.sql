alter table category drop foreign key category_ibfk_2;
drop index IDX_subcategory on category;
alter table category drop column subcategory_id;

drop table subcategory;