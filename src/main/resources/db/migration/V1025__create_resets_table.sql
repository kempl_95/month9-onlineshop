CREATE TABLE resets (
    id INT NOT NULL AUTO_INCREMENT primary key,
    token varchar(128),
    user_id int,
    INDEX IDX_user (user_id),
    FOREIGN KEY FK_user (user_id)
    REFERENCES user (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
);

