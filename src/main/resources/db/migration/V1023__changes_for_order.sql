alter table session
    add column user_id int after number,
    add index IDX_user (user_id),
    add FOREIGN KEY FK_user (user_id) REFERENCES user (id);

