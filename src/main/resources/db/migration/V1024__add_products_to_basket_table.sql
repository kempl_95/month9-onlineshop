alter table basket
    add column product_id int after basket_product_id,
    add index IDX_product (product_id),
    add FOREIGN KEY FK_product (product_id) REFERENCES product (id);

