alter table basket_product
	add column user_id int after product_id,
    add index IDX_user (user_id),
    add FOREIGN KEY FK_user (user_id) REFERENCES user (id),

    add column temp_user_id int after user_id,
    add index IDX_temp_user (temp_user_id),
    add FOREIGN KEY FK_temp_user (temp_user_id) REFERENCES temp_user (id);