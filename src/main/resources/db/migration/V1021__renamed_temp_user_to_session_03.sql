ALTER TABLE `onlineshop`.`basket`
    DROP FOREIGN KEY `basket_ibfk_2`;
ALTER TABLE `onlineshop`.`basket`
    CHANGE COLUMN `temp_user_id` `session_id` INT(11) NULL DEFAULT NULL ;
ALTER TABLE `onlineshop`.`basket`
    ADD CONSTRAINT `basket_ibfk_2`
        FOREIGN KEY (`session_id`)
            REFERENCES `onlineshop`.`session` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE;