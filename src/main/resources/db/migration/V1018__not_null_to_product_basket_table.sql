ALTER TABLE `onlineshop`.`basket_product`
    DROP FOREIGN KEY `basket_product_ibfk_3`,
    DROP FOREIGN KEY `basket_product_ibfk_4`;
ALTER TABLE `onlineshop`.`basket_product`
    CHANGE COLUMN `basket_id` `basket_id` INT(11) NOT NULL ,
    CHANGE COLUMN `temp_user_id` `temp_user_id` INT(11) NOT NULL ;
ALTER TABLE `onlineshop`.`basket_product`
    ADD CONSTRAINT `basket_product_ibfk_3`
        FOREIGN KEY (`temp_user_id`)
            REFERENCES `onlineshop`.`temp_user` (`id`),
    ADD CONSTRAINT `basket_product_ibfk_4`
        FOREIGN KEY (`basket_id`)
            REFERENCES `onlineshop`.`basket` (`id`);