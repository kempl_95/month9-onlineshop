package kg.homework.onlineshop.backend.exception;


import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolationException;

import java.util.Arrays;

import static java.util.stream.Collectors.toList;

@ControllerAdvice(annotations = RestController.class)
public class BindExceptionHandler {

    @ExceptionHandler(BindException.class)
    private ResponseEntity<Object> handleBind(BindException ex){
        var bindingResult = ex.getBindingResult();
        var errors = bindingResult.getFieldErrors().stream()
                //Можно скорректировать ответ тут. И по идее Обработать его в JS
                .map(fe -> String.format("%s -> %s", fe.getField(), fe.getDefaultMessage()))
                .collect(toList());
        return ResponseEntity.unprocessableEntity().body(errors);
    }
    @ExceptionHandler(ConstraintViolationException.class)
    private ResponseEntity<Object> handleConstraintViolationExc(ConstraintViolationException ex){
        var bindingResult = ex.getConstraintViolations();
        var errors = bindingResult.stream()
                //Можно скорректировать ответ тут. И по идее Обработать его в JS
                .map(fe -> String.format("ExecutableParameters: %s -> ConstraintDescriptor: %s -> ExecutableReturnValue: %s-> InvalidValue: %s", Arrays.toString(fe.getExecutableParameters()), fe.getConstraintDescriptor(), fe.getExecutableReturnValue(), fe.getInvalidValue()))
                .collect(toList());
        return ResponseEntity.unprocessableEntity().body(errors);
    }

}
