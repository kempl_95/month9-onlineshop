package kg.homework.onlineshop.backend.dto.frontDTO;

import kg.homework.onlineshop.backend.model.User;
import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PACKAGE)
public class UserResponseDTO {
    private int id;
    private String name;
    private String surname;
    private String email;
    private String login;

    public static UserResponseDTO from(User user) {
        return builder()
                .id(user.getId())
                .name(user.getName())
                .surname(user.getSurname())
                .email(user.getEmail())
                .login(user.getLogin())
                .build();
    }
}
