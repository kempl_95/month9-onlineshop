package kg.homework.onlineshop.backend.dto;

import kg.homework.onlineshop.backend.model.Product;
import kg.homework.onlineshop.backend.model.Review;
import kg.homework.onlineshop.backend.model.User;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class ReviewDTO {
    public static ReviewDTO from(Review review) {
        return builder()
                .id(review.getId())
                .text(review.getText())
                .user(review.getUser())
                .product(review.getProduct())
                .build();
    }

    public static List<ReviewDTO> listFrom(List<Review> objList){
        List<ReviewDTO> listDto = new ArrayList<>();
        objList.stream().forEach(obj -> {
            listDto.add(ReviewDTO.from(obj));
        });
        return listDto;
    }

    private Integer id;
    private String text;
    private User user;
    private Product product;
}
