package kg.homework.onlineshop.backend.dto;

import kg.homework.onlineshop.backend.model.*;
import lombok.*;

import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class BasketDTO {
    public static BasketDTO from1(Basket basket) {
        return builder()
                .id(basket.getId())
                .session(basket.getSession())
                .basketProductList(basket.getBasketProduct())
                .productList(basket.getProductList())
                .sum(basket.getSum())
                .build();
    }
    public static BasketDTO fromWithoutProducts(Basket basket) {
        return builder()
                .id(basket.getId())
                .session(basket.getSession())
                .sum(0.00)
                .build();
    }

    private Integer id;
    private User user;
    private Session session;
    private List<BasketProduct> basketProductList;
    private List<Product> productList;
    private Order order;
    private Double sum;
}
