package kg.homework.onlineshop.backend.dto.frontDTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class ChangePasswordDTO {
    @NotBlank
    @Size(min = 3, max = 128)
    private String newPassword;

    private String email;
}
