package kg.homework.onlineshop.backend.dto.frontDTO;

import lombok.*;

@Getter
@Setter
public class DeleteBP_DTO {
    private String product_id;
    private String basket_id;
}
