package kg.homework.onlineshop.backend.dto;

import kg.homework.onlineshop.backend.model.*;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class BasketProductDTO {
    public static BasketProductDTO from1(BasketProduct bp) {
        return builder()
                .id(bp.getId())
                .session(bp.getSession())
                .product(bp.getProduct())
                .qty(bp.getQty())
                .sum(bp.getSum())
                .build();
    }

    private Integer id;
    private User user;
    private Session session;
    private Product product;
    private Order order;
    private int qty;
    private Double sum;
}
