package kg.homework.onlineshop.backend.dto.frontDTO;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
public class NewUserDTO {
    @NotNull
    @Size(min = 2, message = "length must be min 2 digits")
    @Pattern(regexp = "^[^\\d\\s]+$", message = "Should contain only letters")
    private String name;

    @NotNull
    @Size(min = 2, message = "length must be min 2 digits")
    @Pattern(regexp = "^[^\\d\\s]+$", message = "Should contain only letters")
    private String surname;

    @Email
    @NotBlank
    @Size(min = 2, max = 128, message = "length must be min 2 digits")
    private String email;

    @Size(min = 4, max = 128)
    @NotBlank
    private String login;

    @NotBlank
    @Size(min = 3, max = 128)
    private String password;




}
