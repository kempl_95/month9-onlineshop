package kg.homework.onlineshop.backend.dto;

import kg.homework.onlineshop.backend.model.Product;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class ProductDTO {
    public static ProductDTO from(Product product) {
        return builder()
                .id(product.getId())
                .name(product.getName())
                .image(product.getImage())
                .qty(product.getQty())
                .description(product.getDescription())
                .price(product.getPrice())
                .category(CategoryDTO.from(product.getCategory()))
                .build();
    }
    public static List<ProductDTO> listFrom(List<Product> objList){
        List<ProductDTO> listDto = new ArrayList<>();
        objList.stream().forEach(obj -> {
            listDto.add(ProductDTO.from(obj));
        });
        return listDto;
    }

    private Integer id;
    private String name;
    private String image;
    private int qty;
    private String description;
    private Double price;
    private CategoryDTO category;
}
