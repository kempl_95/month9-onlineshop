package kg.homework.onlineshop.backend.dto.frontDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewReviewDTO {
    private String product_id;
    private String user_email;
    private String text;
}
