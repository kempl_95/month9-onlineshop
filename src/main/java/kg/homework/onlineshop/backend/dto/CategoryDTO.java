package kg.homework.onlineshop.backend.dto;

import kg.homework.onlineshop.backend.model.Category;
import kg.homework.onlineshop.backend.model.Product;
import lombok.*;

import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class CategoryDTO {
    public static CategoryDTO from(Category category) {
        return builder()
                .id(category.getId())
                .name(category.getName())
                .productList(category.getProductList())
                .build();
    }
    public static CategoryDTO formJustList(Category category) {
        return builder()
                .id(category.getId())
                .name(category.getName())
                .build();
    }

    private Integer id;
    private String name;
    private List<Product> productList;
}
