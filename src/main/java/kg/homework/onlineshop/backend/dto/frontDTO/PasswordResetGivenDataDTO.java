package kg.homework.onlineshop.backend.dto.frontDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PasswordResetGivenDataDTO {
    private String token;
    private String email;
}
