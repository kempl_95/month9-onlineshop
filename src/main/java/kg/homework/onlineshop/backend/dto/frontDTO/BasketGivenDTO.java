package kg.homework.onlineshop.backend.dto.frontDTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;

@Getter
@Setter
public class BasketGivenDTO {
    @Min(1)
    private String qty;
    private String product_id;
}
