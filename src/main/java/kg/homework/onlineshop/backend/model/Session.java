package kg.homework.onlineshop.backend.model;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
@Table(name="session")
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 128)
    private String number;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public static Session add(String number) {
        return builder()
                .number(number)
                .build();
    }
}
