package kg.homework.onlineshop.backend.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
@Table(name="basket")
public class Basket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Session session;

    @OneToMany(mappedBy = "id")
    private List<BasketProduct> basketProduct;

    @OneToMany(mappedBy = "id")
    private List<Product> productList;

    @OneToOne
    private Order order;

    @PositiveOrZero
    private double sum;

}
