package kg.homework.onlineshop.backend.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
@Table(name="category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id")
    @OrderBy("name ASC")
    private List<Product> productList;
}
