package kg.homework.onlineshop.backend.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@Table(name="product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(min = 1, max = 255)
    @Column
    private String name;

    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String image;

    @PositiveOrZero
    private int qty;

    @Size(min = 1, max = 48000)
    @Column
    private String description;

    @PositiveOrZero
    private Double price;

    @ManyToOne(fetch = FetchType.LAZY)
    //@JoinColumn(name = "category_id")                 //не обязательно, но можно
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    //@JoinColumn(name = "order_id")                    //не обязательно, но можно
    private Order order;

    @OneToMany(mappedBy = "id")
    List<BasketProduct> BasketProduct;

    @OneToMany(mappedBy = "id")
    List<Review> reviewList;

}
