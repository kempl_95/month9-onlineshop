package kg.homework.onlineshop.backend.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @PositiveOrZero
    @Column(length = 128)
    private Double sum;

    @Column(length = 128)
    private String datetime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "session_id")
    private Session session;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id")
    @OrderBy("name ASC")
    private List<Product> productList;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id")
    private List<Review> reviewList;

    @OneToOne
    private Basket basket;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id")
    private List<BasketProduct> basketProductList;
}
