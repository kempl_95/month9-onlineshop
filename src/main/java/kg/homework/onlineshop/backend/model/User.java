package kg.homework.onlineshop.backend.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
@Table(name="user")
public class User{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(min = 4, max = 128)
    @NotBlank
    @Column(length = 128)
    private String login;

    @Email
    @NotBlank
    @Size(min = 5, max = 128)
    @Column(length = 128)
    private String email;

    @NotBlank
    @Size(min = 3, max = 128)
    @Column(length = 128)
    private String password;

    @NotNull
    @Pattern(regexp = "^[^\\d\\s]+$", message = "Should contain only letters")
    @Column(length = 128)
    private String name;

    @NotNull
    @Pattern(regexp = "^[^\\d\\s]+$", message = "Should contain only letters")
    @Column(length = 128)
    private String surname;

    @Column
    @Builder.Default
    private boolean enabled = true;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    @Builder.Default
    private String role = "USER";

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id")
    private List<Order> orderList;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id")
    private List<Review> reviewList;

}
