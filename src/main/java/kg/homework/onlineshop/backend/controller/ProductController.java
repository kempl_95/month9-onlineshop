package kg.homework.onlineshop.backend.controller;


import kg.homework.onlineshop.backend.dto.ProductDTO;
import kg.homework.onlineshop.backend.services.ProductService;
import kg.homework.onlineshop.frontend.controller.PropertiesService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@RestController
@RequestMapping("/products")
@AllArgsConstructor
public class ProductController {
    @Autowired
    ProductService productService;
    @Autowired
    PropertiesService propertiesService;

    @GetMapping
    public Page<ProductDTO> findProducts(Pageable pageable) {
        return productService.findPageProducts(pageable);
    }
    @GetMapping("/search/{text}")
    public Page<ProductDTO> findPageAllProductsSearch(Pageable pageable, @PathVariable String text) {
        return productService.findPageAllProductsSearch(pageable, text);
    }
    @GetMapping("/search/name/{text}")
    public Page<ProductDTO> findPageAllProductsSearchByName(Pageable pageable, @PathVariable String text) {
        return productService.findPageAllProductsSearchByName(pageable, text);
    }
    @GetMapping("/search/description/{text}")
    public Page<ProductDTO> findPageAllProductsSearchByDescription(Pageable pageable, @PathVariable String text) {
        return productService.findPageAllProductsSearchByDescription(pageable, text);
    }
    @GetMapping("/search/price/{price}")
    public Page<ProductDTO> findPageAllProductsSearchByPrice(Pageable pageable, @PathVariable String price) {
        return productService.findPageAllProductsSearchByPrice(pageable, price);
    }
    @GetMapping("/category/{id}")
    public Page<ProductDTO> getPublicationByCategory(Pageable pageable, @PathVariable String id) {
        return productService.findPageProductsByCategory(pageable, Integer.parseInt(id));
    }
    @GetMapping("/img/{name}")
    public ResponseEntity<byte[]> serveFile(@PathVariable String name) {
        String path = "C:/Work/JAVA/Different/online_shop_img";
        try{
            InputStream is = new FileInputStream(new File(path) + "/" + name);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE)
                    .body(StreamUtils.copyToByteArray(is));
        }catch (Exception e){
            InputStream is = null;
            try {
                is = new FileInputStream(new File(path) + "/" + name);
                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE)
                        .body(StreamUtils.copyToByteArray(is));
            }catch (Exception ex){
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
        return null;
    }
}
