package kg.homework.onlineshop.backend.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@RestController
@RequestMapping("/img")
public class ImageController {

    @GetMapping("/{name}")
    public ResponseEntity<byte[]> serveFile(@PathVariable String name) {
        String path = "C:/Work/JAVA/Different/online_shop_img";
        try{
            InputStream is = new FileInputStream(new File(path) + "/" + name);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE)
                    .body(StreamUtils.copyToByteArray(is));
        }catch (Exception e){
            InputStream is = null;
            try {
                is = new FileInputStream(new File(path) + "/" + name);
                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE)
                        .body(StreamUtils.copyToByteArray(is));
            }catch (Exception ex){
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
        return null;
    }
}
