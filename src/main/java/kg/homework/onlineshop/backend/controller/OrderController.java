package kg.homework.onlineshop.backend.controller;


import kg.homework.onlineshop.backend.dto.frontDTO.BasketIdDTO;
import kg.homework.onlineshop.backend.services.BasketService;
import kg.homework.onlineshop.backend.services.OrderService;
import kg.homework.onlineshop.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.security.Principal;

@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    OrderService orderService;
    @Autowired
    BasketService basketService;
    @Autowired
    UserService userService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void addOrder(@RequestBody BasketIdDTO basketIdDTO, Principal principal, HttpSession session) {
        if (principal != null) {
            orderService.addOrderWithUser(basketIdDTO.getBasket_id(), principal.getName());
            userService.updateOrderDataInUser(userService.getUserByEmail(principal.getName()).getId());
        }else {
            orderService.addOrder(basketIdDTO.getBasket_id()/*, session.getId()*/);
        }
        basketService.updateOrderDataInBasket(Integer.parseInt(basketIdDTO.getBasket_id()));

    }
}
