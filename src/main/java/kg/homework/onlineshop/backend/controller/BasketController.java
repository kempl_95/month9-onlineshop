package kg.homework.onlineshop.backend.controller;


import kg.homework.onlineshop.backend.dto.frontDTO.BasketGivenDTO;
import kg.homework.onlineshop.backend.dto.frontDTO.DeleteBP_DTO;
import kg.homework.onlineshop.backend.repository.BasketProducts;
import kg.homework.onlineshop.backend.repository.Baskets;
import kg.homework.onlineshop.backend.repository.Sessions;
import kg.homework.onlineshop.backend.services.BasketProductService;
import kg.homework.onlineshop.backend.services.BasketService;
import kg.homework.onlineshop.backend.services.ProductService;
import kg.homework.onlineshop.backend.services.SessionService;
import kg.homework.onlineshop.backend.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/baskets")
public class BasketController {
    @Autowired
    BasketService basketService;
    @Autowired
    Baskets baskets;
    @Autowired
    SessionService sessionService;
    @Autowired
    Sessions sessions;
    @Autowired
    BasketProductService basketProductService;
    @Autowired
    BasketProducts basketProducts;
    @Autowired
    ProductService productService;


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void addBasket(@RequestBody BasketGivenDTO basketGivenDTO, Principal principal, HttpSession session) {
        if (session != null) {
            if (session.getAttribute(Constants.BASKET_KEY) == null) {
                session.setAttribute(Constants.BASKET_KEY, new ArrayList<String>());
            }
            try {
                var list = (List<String>) session.getAttribute(Constants.BASKET_KEY);
                list.add(basketGivenDTO.getProduct_id());                                    //Добавляю к сессии
                session.setAttribute(Constants.BASKET_KEY, list);
                sessionService.updateSessionByNumber(session.getId());                //Добавляю temp_user
                //Добавляю/обновляю корзину
                if (principal != null) {
                    if (!basketService.existsBySessionNumber(session.getId()))
                        basketService.addBasketWithUser(session.getId());
                    else
                        basketService.updateBasketWithUser(session.getId());
                } else {
                    if (!baskets.existsBySessionNumber(session.getId()))
                    basketService.addBasket(session.getId());
                }
                //Добавляю/обновляю промежуточную таблицу
                if (!basketProducts.existsByBasketIdAndProductIdAndSessionId(baskets.getBySessionNumber(session.getId()).getId(),
                                                                    Integer.parseInt(basketGivenDTO.getProduct_id()),
                                                                    sessions.getByNumber(session.getId()).getId()))
                    basketProductService.addBasketProduct(basketGivenDTO, session.getId());
                else
                    basketProductService.updateBasketProduct(basketGivenDTO, session.getId());

                basketService.updateBasketProductInBasket(session.getId());
                productService.updateProduct(Integer.parseInt(basketGivenDTO.getProduct_id()));

            } catch (ClassCastException ignored) {
            }
        }
    }
    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void deleteBasketProduct(@RequestBody DeleteBP_DTO deleteBP_dto, HttpSession session) {
        if (session != null) {
            try {
                basketProductService.deleteBasketProductByBasketAndProductId(deleteBP_dto);
                basketService.updateBasketProductInBasket(session.getId());
            } catch (ClassCastException ignored) {
            }
        }
    }
}
