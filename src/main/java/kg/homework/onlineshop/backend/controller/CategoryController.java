package kg.homework.onlineshop.backend.controller;


import kg.homework.onlineshop.backend.dto.CategoryDTO;
import kg.homework.onlineshop.backend.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {
    @Autowired
    CategoryService categoryService;


    @GetMapping
    @ResponseBody
    public List<CategoryDTO> getAllCategories() {
        return categoryService.findCategoryList();
    }

}
