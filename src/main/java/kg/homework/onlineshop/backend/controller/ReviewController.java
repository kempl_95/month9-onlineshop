package kg.homework.onlineshop.backend.controller;


import kg.homework.onlineshop.backend.dto.frontDTO.NewReviewDTO;
import kg.homework.onlineshop.backend.services.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/reviews")
public class ReviewController {
    @Autowired
    ReviewService reviewService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void addReview(@RequestBody NewReviewDTO newReviewDTO, Principal principal) {
        try {
            if (principal != null& newReviewDTO.getUser_email() != null) {
                reviewService.addReview(newReviewDTO);
            }
        } catch (ClassCastException ignored) {
        }
    }
}
