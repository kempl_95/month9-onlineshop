package kg.homework.onlineshop.backend.controller;


import kg.homework.onlineshop.backend.annotations.ApiPageable;
import kg.homework.onlineshop.backend.dto.frontDTO.NewUserDTO;
import kg.homework.onlineshop.backend.dto.UserDTO;
import kg.homework.onlineshop.backend.dto.frontDTO.UserResponseDTO;
import kg.homework.onlineshop.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService userService;

    @ApiPageable
    @GetMapping
    public Slice<UserDTO> findUsers(@ApiIgnore Pageable pageable) {
        return userService.findUsers(pageable);
    }
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserResponseDTO addUser(@RequestBody NewUserDTO userData) {
        return userService.addUser(userData);
    }
}
