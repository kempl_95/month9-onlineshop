package kg.homework.onlineshop.backend.services;

import kg.homework.onlineshop.backend.dto.ReviewDTO;
import kg.homework.onlineshop.backend.dto.frontDTO.NewReviewDTO;
import kg.homework.onlineshop.backend.model.Review;
import kg.homework.onlineshop.backend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewService {

    @Autowired
    Products products;
    @Autowired
    BasketProducts basketProducts;
    @Autowired
    Baskets baskets;
    @Autowired
    Sessions sessions;
    @Autowired
    Reviewes reviewes;
    @Autowired
    Users users;

    public void addReview(NewReviewDTO newReviewDTO){
        reviewes.save(Review.builder()
                .text(newReviewDTO.getText())
                .user(users.getByEmail(newReviewDTO.getUser_email()))
                .product(products.getById(Integer.parseInt(newReviewDTO.getProduct_id())))
                .build()
        );
    }


    public List<ReviewDTO> getReviewListByProductId(Integer id){
        return ReviewDTO.listFrom(reviewes.findAllByProductId(id));
    }
}
