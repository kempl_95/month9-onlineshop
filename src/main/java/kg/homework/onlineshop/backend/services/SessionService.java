package kg.homework.onlineshop.backend.services;


import kg.homework.onlineshop.backend.model.Session;
import kg.homework.onlineshop.backend.repository.Sessions;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SessionService {

    private final Sessions sessions;
    private final PasswordEncoder passwordEncoder;
    private final UserService userService;

    public void updateSessionByNumber(String number){
        if (!sessions.existsByNumber(number)){                                                    //Пользователя нет
            sessions.save(Session.builder()
                    .number(number)
                    .build()
            );
        }
    }
    public void updateSessionByNumberAndUser(String sessionNumber, String userEmail){
        if (!sessions.existsByNumber(sessionNumber)){                                                    //Пользователя нет
            sessions.save(Session.builder()
                    .number(sessionNumber)
                    .user(userService.getUserByEmail(userEmail))
                    .build()
            );
        } else {
            sessions.getByNumber(sessionNumber).setUser(userService.getUserByEmail(userEmail));
        }
    }
}
