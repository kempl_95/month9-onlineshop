package kg.homework.onlineshop.backend.services;

import kg.homework.onlineshop.backend.dto.ProductDTO;
import kg.homework.onlineshop.backend.exception.ResourceNotFoundException;
import kg.homework.onlineshop.backend.model.BasketProduct;
import kg.homework.onlineshop.backend.model.Product;
import kg.homework.onlineshop.backend.repository.BasketProducts;
import kg.homework.onlineshop.backend.repository.Baskets;
import kg.homework.onlineshop.backend.repository.Products;
import kg.homework.onlineshop.backend.repository.Sessions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    Products products;
    @Autowired
    BasketProducts basketProducts;
    @Autowired
    Baskets baskets;
    @Autowired
    Sessions sessions;

    public Page<ProductDTO> findPageProducts(Pageable pageable){
        return products.findAll(pageable)
                .map(ProductDTO::from);
    }
    public List<ProductDTO> getProductsBySession(String sessionNumber){
        List<Product> productList = new ArrayList<>();
        basketProducts.findAllByBasketIdAndSessionId(baskets.getBySessionNumber(sessionNumber).getId(), sessions.getByNumber(sessionNumber).getId()).stream().forEach(basketProduct -> {
            productList.add(products.getById(basketProduct.getProduct().getId()));
        });
        return ProductDTO.listFrom(productList);
    }


    public ProductDTO getProductDTOById(int id) {
        var product = products.findById(id).orElseThrow(ResourceNotFoundException::new);
        return ProductDTO.from(product);
    }
    public Product getProductById(int id) {
        return products.findById(id).orElseThrow(ResourceNotFoundException::new);
    }
    public Page<ProductDTO> findPageAllProductsSearch(Pageable pageable, String text){
        // =========== Вариант 1 - просто поиск по имени
        return products.getProductsByNameLike(pageable, text).map(ProductDTO::from);
        // =========== Вариант 2
        /*
        List<Product> productNameList = products.getProductsByNameLike(pageable, text);
        List<Product> productDescList = products.getProductsByDescriptionLike(pageable, text).getContent();
        if (productNameList.size()>0){
            for (Product product: productNameList){
                productNameList.addAll(productDescList.stream().filter(d -> !d.getId().equals(product.getId())).collect(Collectors.toList()));
            }
            Page<Product> page = new PageImpl<>(productNameList);
            return page.map(ProductDTO::from);
        } else{
            Page<Product> page = new PageImpl<>(productDescList);
            return page.map(ProductDTO::from);
        }*/
        // =========== Вариант СТАРЫЙ, но не бесполезный!
        //List<Product> productNameList = products.getProductsByNameLike(pageable, text);
        //List<Product> productDescList = products.getProductsByDescriptionLike(pageable, text).getContent();

        //List<Product> productPriceList = products.findAllByPriceIsLike(pageable, Double.parseDouble(text)).getContent();
        /*if (productNameList.size()>0){
            for (Product product: productNameList){
                productNameList.addAll(productDescList.stream().filter(d -> !d.getId().equals(product.getId())).collect(Collectors.toList()));
            }*/
            ////productNameList.forEach(product -> {
            ////productNameList.addAll(productDescList.stream().filter(d -> !d.getId().equals(product.getId())).collect(Collectors.toList()));
            //productNameList.addAll(productPriceList.stream().filter(d -> !d.getId().equals(product.getId())).collect(Collectors.toList()));
            ////});
            /*Page<Product> page = new PageImpl<>(productNameList);
            return page.map(ProductDTO::from);
        } else*//* (productDescList.size() > 0)*//* {
            *//*productDescList.forEach(product -> {
                productDescList.addAll(productPriceList.stream().filter(d -> !d.getId().equals(product.getId())).collect(Collectors.toList()));
            });*//*
            Page<Product> page = new PageImpl<>(productDescList);
            return page.map(ProductDTO::from);
        *//*} else {
            Page<Product> page = new PageImpl<>(productPriceList);
            return page.map(ProductDTO::from);
        }*/

    }
    public Page<ProductDTO> findPageAllProductsSearchByName(Pageable pageable, String text){
        return products.getProductsByNameLike(pageable, text).map(ProductDTO::from);
    }
    public Page<ProductDTO> findPageAllProductsSearchByDescription(Pageable pageable, String text){
        return products.getProductsByDescriptionLike(pageable, text).map(ProductDTO::from);
    }

    public Page<ProductDTO> findPageAllProductsSearchByPrice(Pageable pageable, String price){
        return products.findAllByPrice(pageable, Double.parseDouble(price)).map(ProductDTO::from);
    }

    public Page<ProductDTO> findPageProductsByCategory(Pageable pageable, Integer categoryId){
        return products.findAllByCategoryId(pageable, categoryId)
                .map(ProductDTO::from);
    }
    public void updateProduct(Integer productId){
        List<BasketProduct> integerHashSet = new ArrayList<>(basketProducts.getAllByProductId(productId));
        products.getById(productId).setBasketProduct(integerHashSet);
    }

}
