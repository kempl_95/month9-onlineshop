package kg.homework.onlineshop.backend.services;


import kg.homework.onlineshop.backend.model.Order;
import kg.homework.onlineshop.backend.repository.BasketProducts;
import kg.homework.onlineshop.backend.repository.Baskets;
import kg.homework.onlineshop.backend.repository.Orders;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class OrderService {

    private final Orders orders;
    private final Baskets baskets;
    private final UserService userService;
    private final BasketProducts basketProducts;

    public void addOrder(String basket_id/*, String sessionNumber*/){
        var basket = baskets.getById(Integer.parseInt(basket_id));
        /*List<Product> productList = new ArrayList<>();
        basketProducts.getAllByBasketIdAndSessionNumber(basket.getId(), sessionNumber).stream().forEach(basketProduct -> {
            productList.add(basketProduct.getProduct());
        });*/

        orders.save(Order.builder()
                .sum(basket.getSum())
                .datetime(LocalDateTime.now().toString())
                .session(basket.getSession())
                //.productList(productList)
                .basket(basket)
                .build()
        );
    }
    public void addOrderWithUser(String basket_id, String userEmail){
        var user = userService.getUserByEmail(userEmail);
        var basket = baskets.getById(Integer.parseInt(basket_id));
        orders.save(Order.builder()
                .sum(basket.getSum())
                .datetime(LocalDateTime.now().toString())
                .user(user)
                .session(basket.getSession())
                .basket(basket)
                .build()
        );
    }
    public Order getOrderBySessionNumber(String sessionNumber){
        var order = orders.getByBasketId(baskets.getBySessionNumber(sessionNumber).getId());
        order.setBasketProductList(basketProducts.getAllBySessionId(order.getSession().getId()));
        return order;
    }
    public List<Order> getOrdersByUserId(Integer userId){
        return orders.findAllByUserId(userId).stream().peek(order -> {
            order.setBasketProductList(basketProducts.getAllBySessionId(order.getSession().getId()));
        }).collect(Collectors.toList());
    }

}
