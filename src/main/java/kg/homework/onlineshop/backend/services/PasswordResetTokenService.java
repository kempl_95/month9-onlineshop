package kg.homework.onlineshop.backend.services;


import kg.homework.onlineshop.backend.model.PasswordResetToken;
import kg.homework.onlineshop.backend.model.User;
import kg.homework.onlineshop.backend.repository.PasswordResetTokens;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PasswordResetTokenService {
    private final PasswordResetTokens passwordResetTokens;

    //Создание токена
    public void createPasswordResetTokenForUser(User user, String token) {
        passwordResetTokens.deleteAllByUserEmail(user.getEmail());
        passwordResetTokens.save(PasswordResetToken.builder()
                .token(token)
                .user(user)
                .build()
        );
    }
    public PasswordResetToken getPasswordResetTokenByEmail(String email) {
        return passwordResetTokens.getFirstByUserEmail(email);
    }
    public boolean existByToken(String token){
        return passwordResetTokens.existsByToken(token);
    }
    public void deletePasswordResetTokenByEmail(String email){
        passwordResetTokens.deleteAllByUserEmail(email);
    }

}
