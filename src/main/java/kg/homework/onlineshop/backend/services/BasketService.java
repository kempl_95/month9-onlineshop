package kg.homework.onlineshop.backend.services;


import kg.homework.onlineshop.backend.dto.BasketDTO;
import kg.homework.onlineshop.backend.model.Basket;
import kg.homework.onlineshop.backend.model.BasketProduct;
import kg.homework.onlineshop.backend.model.Product;
import kg.homework.onlineshop.backend.repository.BasketProducts;
import kg.homework.onlineshop.backend.repository.Baskets;
import kg.homework.onlineshop.backend.repository.Orders;
import kg.homework.onlineshop.backend.repository.Sessions;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
@AllArgsConstructor
public class BasketService {

    private final Baskets baskets;
    private final SessionService sessionService;
    private final Sessions sessions;
    private final BasketProductService basketProductService;
    private final BasketProducts basketProducts;
    private final OrderService orderService;
    private final Orders orders;

    public BasketDTO getBasketSessionNumber(String sessionNumber){
        return BasketDTO.from1(baskets.getBySessionNumber(sessionNumber));
    }
    public BasketDTO getBasketSessionNumberIfNull(String sessionNumber){
        return BasketDTO.fromWithoutProducts(baskets.getBySessionNumber(sessionNumber));
    }

    public void addBasket(String sessionNumber) {
        baskets.save(Basket.builder()                                                       //Добавляю корзину
                        .session(sessions.getByNumber(sessionNumber))
                        .build()
        );
    }
    public void addBasketWithUser(String sessionNumber) {
        var session = sessions.getByNumber(sessionNumber);
        baskets.save(Basket.builder()                                                       //Добавляю корзину
                .session(session)
                .user(session.getUser())
                .build()
        );
    }
    public void updateBasketWithUser(String sessionNumber) {
        baskets.getBySessionNumber(sessionNumber).setUser(sessions.getByNumber(sessionNumber).getUser());
    }
    public void updateBasketProductInBasket(String sessionNumber) {
        List<BasketProduct> integerHashSet = new ArrayList<>(basketProducts.getAllByBasketId(baskets.getBySessionNumber(sessionNumber).getId()));
        List<Product> productList = new ArrayList<>();
        AtomicReference<Double> totalSum = new AtomicReference<>(0.00);
        basketProducts.getAllByBasketId(baskets.getBySessionNumber(sessionNumber).getId()).forEach(basketProduct -> {
            totalSum.set(totalSum.get() + basketProduct.getSum());
            productList.add(basketProduct.getProduct());
        });
        baskets.getById(baskets.getBySessionNumber(sessionNumber).getId()).setBasketProduct(integerHashSet);
        baskets.getById(baskets.getBySessionNumber(sessionNumber).getId()).setSum(totalSum.get());
        baskets.getById(baskets.getBySessionNumber(sessionNumber).getId()).setProductList(productList);
    }

    public void updateOrderDataInBasket(Integer basketId) {
        baskets.getById(basketId).setOrder(orders.getByBasketId(basketId));
    }




    public boolean existsBySessionNumber(String sessionNumber){
        return baskets.existsBySessionNumber(sessionNumber);
    }


}
