package kg.homework.onlineshop.backend.services;


import kg.homework.onlineshop.backend.dto.frontDTO.ChangePasswordDTO;
import kg.homework.onlineshop.backend.dto.frontDTO.NewUserDTO;
import kg.homework.onlineshop.backend.dto.UserDTO;
import kg.homework.onlineshop.backend.dto.frontDTO.UserResponseDTO;
import kg.homework.onlineshop.backend.exception.UserAlreadyRegisteredException;
import kg.homework.onlineshop.backend.exception.UserNotFoundException;
import kg.homework.onlineshop.backend.model.Order;
import kg.homework.onlineshop.backend.model.User;
import kg.homework.onlineshop.backend.repository.Users;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService {

    private final Users users;
    private final PasswordEncoder passwordEncoder;


    public Slice<UserDTO> findUsers(Pageable pageable) {
        var slice = users.findAll(pageable);
        return slice.map(UserDTO::from);
    }
    public UserResponseDTO addUser(NewUserDTO userData) {
        if (users.existsByEmail(userData.getEmail())) {
            throw new UserAlreadyRegisteredException();
        }
        var user = User.builder()
                .name(userData.getName())
                .surname(userData.getSurname())
                .email(userData.getEmail())
                .login(userData.getLogin())
                .password(passwordEncoder.encode(userData.getPassword()))
                .build();

        users.save(user);

        return UserResponseDTO.from(user);
    }
    public UserResponseDTO getUserResponseByEmail(String email) {
        var user = users.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);

        return UserResponseDTO.from(user);
    }
    public User getUserByEmail(String email) {
        return users.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);
    }
    public boolean existByEmail(String email){
        return users.existsByEmail(email);
    }

    public void changePassword(ChangePasswordDTO changePasswordDTO){
        users.changePassword(passwordEncoder.encode(changePasswordDTO.getNewPassword()), users.getByEmail(changePasswordDTO.getEmail()).getId());
    }
    public void updateOrderDataInUser(Integer userId) {
        List<Order> orderList = new ArrayList<>(users.getById(userId).getOrderList());
        users.getById(userId).setOrderList(orderList);
    }
}
