package kg.homework.onlineshop.backend.services;

import kg.homework.onlineshop.backend.dto.CategoryDTO;
import kg.homework.onlineshop.backend.repository.Categories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {
    @Autowired
    Categories categories;

    public List<CategoryDTO> findCategoryList() {
        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        categories.findAll().forEach(obj ->
                categoryDTOList.add(CategoryDTO.formJustList(obj)));
        return categoryDTOList;
    }

}
