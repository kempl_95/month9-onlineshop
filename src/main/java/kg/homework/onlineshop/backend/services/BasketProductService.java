package kg.homework.onlineshop.backend.services;


import kg.homework.onlineshop.backend.dto.frontDTO.BasketGivenDTO;
import kg.homework.onlineshop.backend.dto.frontDTO.DeleteBP_DTO;
import kg.homework.onlineshop.backend.model.BasketProduct;
import kg.homework.onlineshop.backend.repository.BasketProducts;
import kg.homework.onlineshop.backend.repository.Baskets;
import kg.homework.onlineshop.backend.repository.Products;
import kg.homework.onlineshop.backend.repository.Sessions;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class BasketProductService {

    private final BasketProducts basketProducts;
    private final Baskets baskets;
    private final ProductService productService;
    private final Products products;
    private final Sessions sessions;
    private final SessionService sessionService;

    public void addBasketProduct(BasketGivenDTO basketGivenDTO, String sessionNumber){
        basketProducts.save(BasketProduct.builder()
                .basket(baskets.getBySessionNumber(sessionNumber))
                .product(productService.getProductById(Integer.parseInt(basketGivenDTO.getProduct_id())))
                .session(sessions.getByNumber(sessionNumber))
                .qty(Integer.parseInt(basketGivenDTO.getQty()))
                .sum(productService.getProductById(Integer.parseInt(basketGivenDTO.getProduct_id())).getPrice()*Integer.parseInt(basketGivenDTO.getQty()))
                .build()
        );
    }
    public void updateBasketProduct(BasketGivenDTO basketGivenDTO, String sessionNumber){
        basketProducts.updateBasketProduct(
                Integer.parseInt(basketGivenDTO.getQty()),
                products.getProductById(Integer.parseInt(basketGivenDTO.getProduct_id())).getPrice()*Double.parseDouble(basketGivenDTO.getQty()),
                basketProducts.getByBasketIdAndProductIdAndSessionId(
                        baskets.getBySessionNumber(sessionNumber).getId(),
                        Integer.parseInt(basketGivenDTO.getProduct_id()),
                        sessions.getByNumber(sessionNumber).getId()
                ).getId()
        );
    }
    public List<BasketProduct> getBasketProductsBySessionNumber(String sessionNumber){
        var basket = baskets.getBySessionNumber(sessionNumber);
        return basketProducts.getAllByBasketId(basket.getId());
    }
    public BasketProduct getBasketProductBySessionNumber(Integer productId, String sessionNumber){
        return basketProducts.getFirstByProductIdAndSessionId(productId, sessions.getByNumber(sessionNumber).getId());
    }
    public void deleteBasketProductByBasketAndProductId(DeleteBP_DTO deleteBP_dto){
        basketProducts.deleteByBasketIdAndProductId(Integer.parseInt(deleteBP_dto.getBasket_id()), Integer.parseInt(deleteBP_dto.getProduct_id()));
    }

}
