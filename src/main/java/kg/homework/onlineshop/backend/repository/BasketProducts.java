package kg.homework.onlineshop.backend.repository;


import kg.homework.onlineshop.backend.model.BasketProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface BasketProducts extends JpaRepository<BasketProduct, Integer> {
    boolean existsByProductId (int id);
    boolean existsByBasketIdAndSessionId (Integer basketId, Integer SessionId);
    boolean existsByBasketIdAndProductIdAndSessionId (Integer basketId, Integer productId, Integer sessionId);
    BasketProduct getFirstByProductIdAndSessionId(Integer product_id, Integer session_id);
    BasketProduct getByBasketIdAndProductIdAndSessionId(Integer basketId, Integer productId, Integer sessionId);
    void deleteByBasketIdAndProductId(Integer basketId, Integer productId);
    List<BasketProduct> getAllByBasketId (Integer id);
    List<BasketProduct> getAllByProductId (Integer id);
    List<BasketProduct> getAllBySessionId (Integer id);
    List<BasketProduct> getAllByBasketIdAndSessionNumber (Integer id, String number);
    List<BasketProduct> findAllByBasketIdAndSessionId(Integer basketId, Integer sessionId);

    @Modifying
    @Query("update BasketProduct u set u.qty = :qty, u.sum = :sum where u.id = :id")
    void updateBasketProduct(@Param("qty") int qty, @Param("sum") Double sum, @Param("id") Integer id);
}
