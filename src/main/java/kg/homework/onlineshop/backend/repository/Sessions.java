package kg.homework.onlineshop.backend.repository;

import kg.homework.onlineshop.backend.model.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface Sessions extends JpaRepository<Session, Integer> {
    public boolean existsByNumber(String number);
    Session getByNumber(String number);
    @Modifying
    @Query(value = "insert into Session (number)  values (:number);", nativeQuery = true)
    void insertSession(@Param("number") String number);
}
