package kg.homework.onlineshop.backend.repository;

import kg.homework.onlineshop.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
public interface Users extends JpaRepository<User, Integer> {
    //Поиск пользователя по имени/фамилии/email/логину
    public User getById(Integer id);
    public User findByName(String name);
    public User findBySurname(String surname);
    public User findByNameAndSurname(String name, String surname);
    Optional<User> findByEmail(String email);
    public User findByLogin(String login);
    public Optional<User> getByLogin(String s);
    public User getByEmail(String s);

    //OLD
    //при регистрации нового пользователя, проверить по email, существует ли такой-же
    public boolean existsByEmail(String email);
    public boolean existsByLogin(String login);
    //при входе (login) проверить email и password на совпадение
    public User getFirstByEmailAndPassword(String email, String password);
    public boolean existsByEmailAndPassword(String email, String password);
    //при входе (login) проверить login и password на совпадение
    public User getFirstByLoginAndPassword(String login, String password);
    public boolean existsByLoginAndPassword(String login, String password);

    @Modifying
    @Query("update User u set u.password = :password where u.id = :id")
    void changePassword(@Param("password") String password, @Param("id") Integer id);
}
