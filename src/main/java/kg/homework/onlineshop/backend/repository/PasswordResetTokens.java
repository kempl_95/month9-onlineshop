package kg.homework.onlineshop.backend.repository;


import kg.homework.onlineshop.backend.model.PasswordResetToken;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface PasswordResetTokens extends JpaRepository<PasswordResetToken, Integer> {
    void deleteAllByUserEmail(String email);
    PasswordResetToken getFirstByUserEmail(String email);
    boolean existsByToken(String token);
}
