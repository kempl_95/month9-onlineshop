package kg.homework.onlineshop.backend.repository;

import kg.homework.onlineshop.backend.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

//@Transactional
public interface Orders extends JpaRepository<Order, Integer> {
    List<Order> findAllBySessionNumber(String number);
    List<Order> findAllByUserId(Integer id);
    Order getByBasketId(Integer id);
    Order getByBasketIdAndUserId(Integer basketId, Integer userId);
    //Order getFirstByUserIdAndSessionNumberOrderByDatetime(Integer userId, String number);

/*
    @Modifying
    @Query("update BasketProduct u set u.qty = :qty, u.sum = :sum where u.id = :id")
    Order getFirstByUserIdAndSessionNumberOrderByDatetime(@Param("qty") int userId, @Param("sum") String number);*/
}
