package kg.homework.onlineshop.backend.repository;


import kg.homework.onlineshop.backend.model.Basket;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface Baskets extends JpaRepository<Basket, Integer> {
    public boolean existsBySessionNumber(String number);
    Basket getBySessionNumber(String number);
    Basket getByUserId(Integer id);
    Basket getById(Integer id);
    void deleteAllById(Integer id);

}
