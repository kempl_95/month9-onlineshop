package kg.homework.onlineshop.backend.repository;

import kg.homework.onlineshop.backend.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface Reviewes extends JpaRepository<Review, Integer> {
    List<Review> findAllByProductId(int id);
}
