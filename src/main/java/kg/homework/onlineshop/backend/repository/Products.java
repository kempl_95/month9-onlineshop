package kg.homework.onlineshop.backend.repository;

import kg.homework.onlineshop.backend.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Set;

@Transactional
public interface Products extends JpaRepository<Product, Integer> {
    Page<Product> findAll (Pageable pageable);
    Page<Product> findAllByCategoryId (Pageable pageable, Integer id);
    @Query("select p from Product as p where p.name like CONCAT('%', :text, '%') ")
    Page<Product> getProductsByNameLike (Pageable pageable, String text);
    @Query("select p from Product as p where p.description like CONCAT('%', :text, '%') ")
    Page<Product> getProductsByDescriptionLike (Pageable pageable, String text);
    Page<Product> findAllByPrice (Pageable pageable, Double price);
    Product getProductById(Integer id);
    Set<Product> getAllById(Integer id);
    Product getById(Integer id);
}
