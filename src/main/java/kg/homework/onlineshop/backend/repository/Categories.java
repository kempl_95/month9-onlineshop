package kg.homework.onlineshop.backend.repository;

import kg.homework.onlineshop.backend.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Categories extends JpaRepository<Category, Integer> {

}
