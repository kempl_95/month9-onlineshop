package kg.homework.onlineshop.frontend.controller;

import kg.homework.onlineshop.backend.utils.Constants;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class BasketAdvice {


    @ModelAttribute(Constants.BASKET_KEY)
    public List<String> getCartModel(HttpSession session) {
        var list = session.getAttribute(Constants.BASKET_KEY);
        if (list == null) {
            session.setAttribute(Constants.BASKET_KEY, new ArrayList<>());
        }
        return (List<String>) session.getAttribute(Constants.BASKET_KEY);
    }

}
