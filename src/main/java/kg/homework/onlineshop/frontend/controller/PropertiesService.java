package kg.homework.onlineshop.frontend.controller;

import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PropertiesService {
    private final SpringDataWebProperties pageableDefaultProps;

    public int getDefaultPageSize() {
        return pageableDefaultProps.getPageable().getDefaultPageSize();
    }
    public void setPageSize(int size) {
        pageableDefaultProps.getPageable().setDefaultPageSize(size);
    }
    public void setDefaultPageSize(){
        setPageSize(pageableDefaultProps.getPageable().getDefaultPageSize());
    }

}
