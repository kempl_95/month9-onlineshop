package kg.homework.onlineshop.frontend.controller;

import kg.homework.onlineshop.backend.dto.frontDTO.ChangePasswordDTO;
import kg.homework.onlineshop.backend.dto.frontDTO.ForgotPasswordDTO;
import kg.homework.onlineshop.backend.dto.frontDTO.NewUserDTO;
import kg.homework.onlineshop.backend.dto.frontDTO.PasswordResetGivenDataDTO;
import kg.homework.onlineshop.backend.exception.ResourceNotFoundException;
import kg.homework.onlineshop.backend.services.*;
import kg.homework.onlineshop.backend.utils.Constants;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping
@AllArgsConstructor
public class FrontController {

    private final PropertiesService propertiesService;
    private final CategoryService categoryService;
    private final ProductService productService;
    private final UserService userService;
    private final BasketService basketService;
    private final BasketProductService basketProductService;
    private final SessionService sessionService;
    private final PasswordResetTokenService passwordResetTokenService;
    private final PasswordEncoder passwordEncoder;
    private final OrderService orderService;
    private final ReviewService reviewService;


    @GetMapping
    public String root(Model model, Pageable pageable, Principal principal, HttpSession session) {
        model.addAttribute("categories", categoryService.findCategoryList());
        model.addAttribute("products", productService.findPageProducts(pageable).getContent());
        if (principal != null){
            var userResponse = userService.getUserResponseByEmail(principal.getName());
            model.addAttribute("dto", userResponse);
            sessionService.updateSessionByNumberAndUser(session.getId(), principal.getName());                      //Добавляю session
            //Присваиваю/обновляю session_id к user
            //Добавляю/обновляю корзину
            if (!basketService.existsBySessionNumber(session.getId()))
                basketService.addBasketWithUser(session.getId());
            else
                basketService.updateBasketWithUser(session.getId());
        } else {
            if (session != null) {
                if (session.getAttribute(Constants.BASKET_KEY) == null) {
                    session.setAttribute(Constants.BASKET_KEY, new ArrayList<String>());
                }
                try {
                    var list = (List<String>) session.getAttribute(Constants.BASKET_KEY);
                    session.setAttribute(Constants.BASKET_KEY, list);
                    sessionService.updateSessionByNumber(session.getId());              //Добавляю session
                    //Добавляю/обновляю корзину
                    if (!basketService.existsBySessionNumber(session.getId()))
                        basketService.addBasket(session.getId());
                    else
                        basketService.updateBasketProductInBasket(session.getId());
                } catch (ClassCastException ignored) {
                }
            }
        }
        return "homepage";
    }


    @GetMapping("/products/{id:\\d+?}")
    public String productPage(@PathVariable int id, Model model, Principal principal, HttpSession session) {
        model.addAttribute("product", productService.getProductById(id));
        model.addAttribute("categories", categoryService.findCategoryList());
        if (principal != null){
            var user = userService.getUserResponseByEmail(principal.getName());
            model.addAttribute("dto", user);
        }
        model.addAttribute("reviewList", reviewService.getReviewListByProductId(id));

        return "product";
    }
    @GetMapping("/basket")
    public String basket(Model model, Principal principal, HttpSession session) {
        try {
            model.addAttribute("basket", basketService.getBasketSessionNumber(session.getId()));
        }catch (NullPointerException ex){
            model.addAttribute("basket", basketService.getBasketSessionNumberIfNull(session.getId()));
        }
        model.addAttribute("basket_products", basketProductService.getBasketProductsBySessionNumber(session.getId()));
        model.addAttribute("categories", categoryService.findCategoryList());
        if (principal != null){
            var user = userService.getUserResponseByEmail(principal.getName());
            model.addAttribute("dto", user);
        }
        return "basket";
    }
    @GetMapping("/success_order")
    public String success_order(Model model, Principal principal, HttpSession session) {
        model.addAttribute("categories", categoryService.findCategoryList());
        if (principal != null){
            var user = userService.getUserResponseByEmail(principal.getName());
            model.addAttribute("dto", user);
            model.addAttribute("order", orderService.getOrderBySessionNumber(session.getId()));
        }else {
            model.addAttribute("order", orderService.getOrderBySessionNumber(session.getId()));
            //session.invalidate();

        }
        return "success_order";
    }
    @GetMapping("/profile")
    public String profile(Model model, Principal principal, HttpSession session) {
        model.addAttribute("categories", categoryService.findCategoryList());
        if (principal != null){
            var user = userService.getUserResponseByEmail(principal.getName());
            model.addAttribute("dto", user);
            //model.addAttribute("order", orderService.getOrdersByUserId(userService.getUserByEmail(principal.getName()).getId()));
            model.addAttribute("orders", orderService.getOrdersByUserId(user.getId()));
        }
        return "profile";
    }

    // РЕГИСТРАЦИЯ
    @GetMapping("/registration")
    public String reg(Model model) {
        if (!model.containsAttribute("dto")) {
            model.addAttribute("dto", new NewUserDTO());
        }
        return "registration";
    }
    @PostMapping("/registration")
    public String registerPage(@Valid NewUserDTO newUserDTO,
                               BindingResult validationResult,
                               RedirectAttributes attributes) {
        attributes.addFlashAttribute("dto", newUserDTO);

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/registration";
        }

        userService.addUser(newUserDTO);
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String log(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
        /*if (!model.containsAttribute("dto")) {
            model.addAttribute("dto", new NewUserDTO());
        }*/
        model.addAttribute("error", error);
        return "login";
    }

    //ВОСТАНОВЛЕНИЕ ПАРОЛЯ
    @GetMapping("/forgot_password")
    public String forgotPasswordGet(Model model) {
        if (!model.containsAttribute("dto")) {
            model.addAttribute("dto", new ForgotPasswordDTO());
        }
        return "forgot_password";
    }
    @PostMapping("/forgot_password")
    public String resetPassword(@Valid ForgotPasswordDTO forgotPasswordDTO,
                                BindingResult validationResult,
                                RedirectAttributes attributes) {
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/forgot_password";
        }
        if (!userService.existByEmail(forgotPasswordDTO.getEmail())){
            attributes.addFlashAttribute("errorText", "Введенный email не существует");
            return "redirect:/forgot_password";
        }
        String token = UUID.randomUUID().toString();
        var user = userService.getUserByEmail(forgotPasswordDTO.getEmail());
        passwordResetTokenService.createPasswordResetTokenForUser(user, token);
        //тут как бы отправка на почту этого token, затем пользователь на странице /emulation его введет
        return "redirect:/emulation/".concat(forgotPasswordDTO.getEmail());
    }
    @GetMapping("/emulation/{email}")
    public String emulation(Model model, @PathVariable("email") String email) {
        //Мы как бы получили токен на почту
        if (!model.containsAttribute("dto")) {
            var prgdDto = new PasswordResetGivenDataDTO();
            var prt =  passwordResetTokenService.getPasswordResetTokenByEmail(email);
            prgdDto.setToken(prt.getToken());
            prgdDto.setEmail(email);
            model.addAttribute("dto", prgdDto);
        }
        return "emulation";
    }
    @PostMapping("/emulation")
    public String  checkToken(@Valid PasswordResetGivenDataDTO passwordResetGivenDataDTO,
                              BindingResult validationResult,
                              RedirectAttributes attributes) {
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/emulation/".concat(passwordResetGivenDataDTO.getEmail());
        }
        if (!passwordResetTokenService.existByToken(passwordResetGivenDataDTO.getToken())){
            attributes.addFlashAttribute("errorText", "Введенный token неверный");
            return "redirect:/emulation/".concat(passwordResetGivenDataDTO.getEmail());
        }
        //Раз токен введен верно, его можно удалить и перенаправить на восстановление пароля
        passwordResetTokenService.deletePasswordResetTokenByEmail(passwordResetGivenDataDTO.getEmail());
        return "redirect:/reset/".concat(passwordResetGivenDataDTO.getEmail());
    }
    @GetMapping("/reset/{email}")
    public String resetPassword(Model model, @PathVariable("email") String email) {
        if (!model.containsAttribute("dto")) {
            var changePasswordDTO = new ChangePasswordDTO();
            changePasswordDTO.setEmail(email);
            model.addAttribute("dto", changePasswordDTO);
        }
        model.addAttribute("email",  email);
        return "reset";
    }

    @PostMapping("/reset")
    public String registerPage(@Valid ChangePasswordDTO changePasswordDTO,
                               BindingResult validationResult,
                               RedirectAttributes attributes) {
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/reset";
        }
        userService.changePassword(changePasswordDTO);
        return "redirect:/login";
    }
    //ВОСТАНОВЛЕНИЕ ПАРОЛЯ

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    private String handleRNF (ResourceNotFoundException ex){
        return "resource-not-found";
    }
    //Потом переделать вывод ошибки
    @ExceptionHandler(ClassCastException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    private String handleCCE (ClassCastException ex){
        return "resource-not-found";
    }
    @ExceptionHandler(NullPointerException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    private String handleNullPointerEx (NullPointerException ex){
        return "resource-not-found";
    }






    /*private static <T> void fillPaginationDataModel(Page<T> list, int pageSize, Model model, String baseUri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink", constructPageUri(baseUri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }

        if (list.hasPrevious()) {
            model.addAttribute("prevPageLink", constructPageUri(baseUri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }

        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }
    private static String constructPageUri(String uri, int page, int size) {
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }*/

    /*@GetMapping
    public String root(Model model, Pageable pageable, HttpServletRequest uriBuilder) {
        var categories = categoryService.findCategoryList();
        model.addAttribute("categories", categories);
        var products = productService.findPageProducts(pageable);
        var uri = uriBuilder.getRequestURI();
        fillPaginationDataModel(products, propertiesService.getDefaultPageSize(), model, uri);
        return "index";
    }
    @GetMapping("/search/{text}")
    public String placePage(@PathVariable String text, Model model, Pageable pageable, HttpServletRequest uriBuilder) {
        var categories = categoryService.findCategoryList();
        model.addAttribute("categories", categories);
        var products = productService.findPageProducts(pageable);
        var uri = uriBuilder.getRequestURI();
        fillPaginationDataModel(products, propertiesService.getDefaultPageSize(), model, uri);
        return "search_result";
    }
*/
    /*@GetMapping("/products/{id:\\d+?}")
    public String placePage(@PathVariable int id, Model model, Pageable pageable, HttpServletRequest uriBuilder) {
        model.addAttribute("place", placeService.getPlace(id));
        var uri = uriBuilder.getRequestURI();
        var foods = foodService.getFoods(id, pageable);
        fillPaginationDataModel(foods, propertiesService.getDefaultPageSize(), model, uri);

        return "place";
    }*/

}
